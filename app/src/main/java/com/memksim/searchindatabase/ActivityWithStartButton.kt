package com.memksim.searchindatabase

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import com.memksim.searchindatabase.databinding.ActivityWithStartButtonBinding
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit

class ActivityWithStartButton : AppCompatActivity() {
    companion object {
        const val NUMBER_MESSAGE = 1
    }

    private val handler = Handler(Looper.getMainLooper()) {

        if (it.what == NUMBER_MESSAGE) {
            checkNumber(it.obj as Int)
        }

        true
    }

    private lateinit var binding: ActivityWithStartButtonBinding

    fun checkNumber(num: Int) {
        if ((num % 10) == 0) {
            binding.textView.text = num.toString()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWithStartButtonBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        //Log.d(MainActivity.TAG, "onCreate: starting...")


        val bag = CompositeDisposable()

        val colors = Observable.just("red", "blue", "green", "purple")
            .delay(1, TimeUnit.MILLISECONDS)
        val nums = Observable.just("1", "2", "3")


        binding.startButton.setOnClickListener { view ->

            bag.addAll(
                colors

                    .subscribe(){
                        Log.d(TAG, "onCreate: $it")
                    }
                    
            )

        }

        binding.stopButton.setOnClickListener {
            Log.d(TAG, "onCreate: clear bag")
            bag.clear()

        }

    }

    fun getData(): Observable<Int>{
        return Observable.create {
            for(i in 0..20){
                it.onNext(i)
            }
        }
    }

}




