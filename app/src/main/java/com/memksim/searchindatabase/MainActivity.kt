package com.memksim.searchindatabase

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.memksim.searchindatabase.databinding.ActivityMainBinding
import com.memksim.searchindatabase.viewmodel.MainActivityViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import com.jakewharton.rxbinding4.widget.textChanges
import java.util.concurrent.TimeUnit

const val TAG = "test"
class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    lateinit var viewModel: MainActivityViewModel
    lateinit var adapter: UserAdapter
    private var request: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        Log.d(TAG, "onCreate: starting...")
        viewModel = ViewModelProvider(this)[MainActivityViewModel::class.java]

        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = UserAdapter()
        binding.recyclerView.adapter = adapter

        viewModel.getData()

        viewModel.liveData.observe(this, {
            adapter.notifyDataSetChanged()
            adapter.list = it
            adapter.notifyDataSetChanged()
        })

        binding.et.textChanges()
            .delay(500, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .debounce(500, TimeUnit.MILLISECONDS)
            .subscribe ({
                //Log.d(TAG, "onCreate: $it")
                viewModel.findMatches(it.toString())

            },{
                Log.d(TAG, "onCreate: $it")
            })
            

        binding.fill.setOnClickListener {
            viewModel.newUser()
        }
        
        binding.stop.setOnClickListener {
            Log.d(TAG, "onCreate: stop")
            viewModel.stop()
        }

        binding.find.setOnClickListener {
            Log.d(TAG, "onCreate: $request")
            viewModel.findMatches(request)
        }

    }

    override fun onStart() {
        super.onStart()
    }
}