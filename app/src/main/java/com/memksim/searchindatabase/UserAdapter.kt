package com.memksim.searchindatabase

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.memksim.searchindatabase.data.User
import com.memksim.searchindatabase.databinding.ItemUserBinding

class UserAdapter: RecyclerView.Adapter<UserAdapter.UserViewHolder>() {

    private lateinit var context: Context
    var list: List<User> = emptyList()

    class UserViewHolder(
        val binding: ItemUserBinding
    ): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        context = parent.context
        val inflater = LayoutInflater.from(context)
        val binding = ItemUserBinding.inflate(inflater, parent, false)

        val viewHolder = UserViewHolder(binding)

        return viewHolder
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {

        holder.binding.userId.text = list[position]._id.toString()
        holder.binding.userName.text = "${list[position].username} ${list[position].surname}"
        holder.binding.userAge.text = "${list[position].age} y.o."

    }

    override fun getItemCount(): Int {
        return list.size
    }

}