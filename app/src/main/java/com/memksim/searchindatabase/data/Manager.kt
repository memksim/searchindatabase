package com.memksim.searchindatabase.data

import android.content.Context
import android.util.Log
import com.github.javafaker.Faker
import com.memksim.searchindatabase.TAG
import io.reactivex.rxjava3.core.Observable

class Manager(private val context: Context) {

    private val database = UserDatabase.getDataBase(context)
    private val dao = database.userDao()

    fun fillDataBase(){
        val faker = Faker()
        val user: User = User(
            0,
            faker.name().firstName(),
            faker.name().lastName(),
            faker.number().numberBetween(0, 100)
        )
        dao.insert(user)
        Log.d(TAG, "fillDataBase: add element")

    }

    fun getUsersList(): Observable<List<User>>{
        return dao.getUsers()
    }



}