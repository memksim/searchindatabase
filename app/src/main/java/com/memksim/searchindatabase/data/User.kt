package com.memksim.searchindatabase.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users")
data class User(
    @PrimaryKey(autoGenerate = true) val _id: Int,
    val username: String,
    val surname: String,
    val age: Int
)
