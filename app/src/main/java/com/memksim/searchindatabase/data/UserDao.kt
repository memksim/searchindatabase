package com.memksim.searchindatabase.data

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import io.reactivex.rxjava3.core.Observable

@Dao
interface UserDao {

    @Insert
    fun insert(vararg users: User)

    @Query("SELECT * FROM users")
    fun getUsers(): Observable<List<User>>

    @Delete
    fun delete(vararg users: User)

}