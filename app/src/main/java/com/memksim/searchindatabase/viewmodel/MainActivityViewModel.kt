package com.memksim.searchindatabase.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.memksim.searchindatabase.TAG
import com.memksim.searchindatabase.data.Manager
import com.memksim.searchindatabase.data.User
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import java.util.*
import io.reactivex.rxjava3.core.*
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlin.collections.ArrayList

class MainActivityViewModel(
    application: Application
): AndroidViewModel(application) , MainActivityViewModelContract {

    private val manager = Manager(application)
    private val disposableBag = CompositeDisposable()

    private val _data: MutableLiveData<Observable<List<User>>>  by lazy {
        MutableLiveData<Observable<List<User>>>(manager.getUsersList())
    }
    val liveData: MutableLiveData<List<User>> by lazy {
        MutableLiveData()
    }

    private var usersList: MutableList<User> = ArrayList()

    override fun newUser() {
        disposableBag.add(Observable.create<User>
            {
                while(!it.isDisposed){
                     manager.fillDataBase()
                }
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
        )
    }

    override fun getData(){
        Log.d(TAG, "set Data for _data")
        _data.value = manager.getUsersList()
        Log.d(TAG, "getData: ${_data.value}")
        fillLiveData()
    }

    override fun stop() {
        Log.d(TAG, "stop:dispose bag ")
        disposableBag.clear()
        getData()
    }

    override fun fillLiveData(){

        val list: MutableList<User> = ArrayList()
        val data = _data.value!!
        data
            .subscribeOn(Schedulers.newThread())
            .flatMap { 
                Observable.fromIterable(it)
            }
            .subscribe({
                //Log.d(TAG, "fillLiveData: $it")
                list.add(it)

            },{

            },{
                Log.d(TAG, "fillLiveData: filled!")

            })

        liveData.value = list
        usersList = list
    }

    override fun findMatches(request: String) {
        val list: MutableList<User> = ArrayList()
        val data = Observable.fromIterable(usersList)
        data
            .doOnSubscribe {
                Log.d(TAG, "findMatches: Subscribed!")
            }
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .filter {
                (it.surname.contains(request, ignoreCase = true) || it.username.contains(request, ignoreCase = true))
            }
            .subscribe({ it ->
                Log.d(TAG, "findMatches: $it")
                list.add(it)
            },{

            },{
                Log.d(TAG, "findMatches: find some matches")
                Log.d(TAG, "findMatches: ${list}")
                liveData.value = list
            })


    }


}