package com.memksim.searchindatabase.viewmodel

interface MainActivityViewModelContract {

    fun newUser()

    fun getData()

    fun stop()

    fun fillLiveData()

    fun findMatches(request: String)

}